package mx.tecnm.misantla.myappregistro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        // Obtenemos el bundle
        val bundle : Bundle? = intent.extras

       /*
       *  val bundeleRecepcion = intent.extras
       * if(bundleRecepcion != null){
       *  val nombres = bundleRecepcion.getString("KEY_NOMBRES")
       * val nombres = bundleRecepcion!!.getString("KEY_NOMBRES") --> si llega un nulo es mi responsabilidad,
       *  si llega un nulo se va a caer.
       *
       * val edad = bundleRecepcion.getString("KEY_EDAD")
       * val genero = bundleRecepcion.getString("KEY_GENERO")
       *
       * tvNombreResultado.text = nombres
       * tvEdadResultado.text= edad
       * tvGeneroResultado.text = genero
       *
       * */

        /*
        *
        *
        * */


        //let
        bundle?.let {bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombres","Desconocido")
            val edad = bundleLibriDeNull.getString("key_edad","0")
            val genero = bundleLibriDeNull.getString("key_genero","")

            tvNombreDestino.text = "Nombres: $nombres"
            tvEdadDestino.text = "Edad: $edad"
            tvGeneroDestino.text = "Genero: $genero"
        }
    }
}