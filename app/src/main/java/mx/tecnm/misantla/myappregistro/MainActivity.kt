package mx.tecnm.misantla.myappregistro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.ayuda.*
import kotlinx.android.synthetic.main.ayuda.view.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 1.- Evento OnClick
        btnEnviar.setOnClickListener{
            // 2.- Obtener la informacion de las cajas de texto

            val nombres = edtNombre.text.toString()
            val edad = edtEdad.text.toString()

            //3.- validando que los nombres no estan vacios

            if(nombres.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus nombres",Toast.LENGTH_LONG).show()
                 return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }
            // 4.- Validando que la edad no este vacia
            if(edad.isEmpty()){
                Toast.makeText(this,"Debe ingresar su edad",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }

            // 5.- validadndo que haya aceptado los terminos y condiciones

            if(!chkTerminos.isChecked){
                Toast.makeText(this,"Debe aceptar los terminos",Toast.LENGTH_LONG).show()
                return@setOnClickListener    // deberia deternese el codigo y vuelva a solicitar
            }

            var genero = if(rdbMasculino.isChecked)"Masculino" else "Femenino"

            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres",nombres)
                putString("key_edad",edad)
                putString("key_genero",genero)
            }
            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }

        //Textview Ayuda

        tvAyuda.setOnClickListener {
          val bottomSheet = BottomSheetDialog(this,R.style.BottomSheetDialogTheme)

            // inflar la vista
            val view = LayoutInflater.from(this).inflate(R.layout.ayuda, contenedor)

            view.tvTelefono.text ="Hola"

            bottomSheet.setContentView(view)
            bottomSheet.show()
        }

    }
}